import axios from "axios";
import "./style.css"

const login = localStorage.getItem("login")
const password = localStorage.getItem("password")
let regime = true
const renderLogin = () => {

    const main = document.querySelector("main")
    if (regime) {
        const loginPage = document.createElement("div")
        loginPage.classList.add("login")

        const user = document.createElement("input")
        user.placeholder = "user"
        const password = document.createElement("input")
        password.placeholder = "password"

        const btn = document.createElement("button")
        btn.innerText = "Login"
        btn.addEventListener('click', () => {
            axios.post("https://news-blog-server-z5lf.onrender.com/auth", {username: user.value, password: password.value}).then(res => {
                if (res.data === "go") {
                    localStorage.setItem("login", user.value)
                    localStorage.setItem("password", password.value)
                    go()
                }
            })
        })
        loginPage.appendChild(user)
        loginPage.appendChild(password)
        loginPage.appendChild(btn)
        main.innerHTML = ""
        main.appendChild(loginPage)
    } else {
        const registerPage = document.createElement("div")
        registerPage.classList.add("login")

        const user = document.createElement("input")
        user.placeholder = "user"
        const password = document.createElement("input")
        password.placeholder = "password"

        const btn = document.createElement("button")
        btn.innerText = "Register"
        btn.addEventListener('click', () => {
            axios.post("https://news-blog-server-z5lf.onrender.com/auth/register", {
                username: user.value,
                password: password.value,
                role: "admin"
            }).then(res => {
                if (res.data === "go") {
                    localStorage.setItem("login", user.value)
                    localStorage.setItem("password", password.value)
                    go()
                }
            })
        })
        registerPage.appendChild(user)
        registerPage.appendChild(password)
        registerPage.appendChild(btn)
        main.innerHTML = ""
        main.appendChild(registerPage)
    }

    const regimeSwitcher = document.createElement("button")
    regimeSwitcher.classList.add("regime")
    regimeSwitcher.innerText = regime ? "register" : "login"
    regimeSwitcher.addEventListener("click", () => {
        regime = !regime
        renderLogin()
    })
    main.appendChild(regimeSwitcher)
}
const go = async () => {
    const login = localStorage.getItem("login")
    const password = localStorage.getItem("password")
    if (login && password) {
        let go;
        await axios.post("https://news-blog-server-z5lf.onrender.com/auth", {userName: login, password: password}).then(res => {
            if (res.data === "go") {
                go = true
            }
        })
        if (go) {
            const start = () => {
                const main = document.querySelector("main")
                main.innerHTML = ""

                axios.get("https://news-blog-server-z5lf.onrender.com/posts").then(res => {
                    render(res.data)
                })

                const getTags = async (id) => {
                    let tags = [];
                    await axios.get("https://news-blog-server-z5lf.onrender.com/tags/" + id).then(res => {
                        if (res.data) {
                            tags = res.data
                        }
                    })
                    return tags || []
                }
                "×"
                const render = (posts) => {
                    posts.forEach(async (e) => {
                        const post = document.createElement("div")
                        post.classList.add("post")

                        const deleteBtn = document.createElement("button")
                        deleteBtn.innerText = "×"
                        deleteBtn.addEventListener('click', () => {
                            axios.delete("https://news-blog-server-z5lf.onrender.com/posts/" + e.id).then(() => {
                                start()
                            })
                        })
                        deleteBtn.classList.add("delete")

                        const title = document.createElement("h3")
                        title.innerText = e.title

                        const author = document.createElement("p")
                        author.innerText = "author: " + e.author
                        author.classList.add("author")

                        const text = document.createElement("p")
                        text.innerText = e.text
                        text.classList.add("text")

                        const image = document.createElement("img")
                        image.src = e.image


                        post.appendChild(title)
                        post.appendChild(author)
                        post.appendChild(text)
                        post.appendChild(image)
                        post.appendChild(deleteBtn)

                        const tags = await getTags(e.id)
                        const tagsEl = document.createElement("div")
                        tagsEl.classList.add("tags")
                        tags?.forEach(tag => {
                            const tagEl = document.createElement("p")
                            tagEl.innerText = "#" + tag.text
                            tagEl.classList.add("tag")
                            tagsEl.appendChild(tagEl)
                        })

                        post.appendChild(tagsEl)
                        main.appendChild(post)
                    })
                }

            }

            start()

            const addBtn = document.querySelector(".addPost")

            const renderModal = () => {
                const modal = document.querySelector(".modal")
                modal.innerHTML = ""
                modal.classList.add("visible")
                modal.addEventListener('click', () => {
                    modal.classList.remove("visible")
                })
                const form = document.createElement("form")
                form.addEventListener('click', (e) => {
                    e.preventDefault()
                    e.stopPropagation()
                })
                const closeButton = document.createElement("button")
                closeButton.classList.add("delete")
                closeButton.innerText = "×"
                closeButton.addEventListener("click", () => {
                    modal.classList.remove("visible")
                })

                form.appendChild(closeButton)

                const name = document.createElement("input")
                name.placeholder = "name"
                form.appendChild(name)

                const author = document.createElement("input")
                author.placeholder = "author"
                form.appendChild(author)


                const text = document.createElement("textarea")
                text.placeholder = "text"
                form.appendChild(text)

                const image = document.createElement("input")
                image.placeholder = "image url"
                form.appendChild(image)

                const tags = document.createElement("div")
                tags.classList.add("tagsForm")

                const tagsArr = []

                const tagsAdd = document.createElement("div")
                tagsAdd.classList.add("tagsAdd")

                const tagInput = document.createElement("input")
                tagInput.placeholder = "tagName"

                const tagBtn = document.createElement("button")
                tagBtn.innerText = "Add Tag"
                tagBtn.addEventListener("click", () => {
                    tagsArr.push(tagInput.value)
                    tagInput.value = ""
                    renderTags()
                })
                const tagsBlock = document.createElement("div")
                tagsBlock.classList.add("tagsBlock")

                const renderTags = () => {
                    tagsBlock.innerText = ""
                    tagsArr.forEach(e => {
                        const tagElement = document.createElement("p")
                        tagElement.innerText = "#" + e
                        tagsBlock.appendChild(tagElement)
                    })
                }
                tagsAdd.appendChild(tagInput)
                tagsAdd.appendChild(tagBtn)
                tags.appendChild(tagsAdd)
                tags.appendChild(tagsBlock)
                form.appendChild(tags)

                const addFormBtn = document.createElement("button")
                addFormBtn.classList.add("addFormBtn")
                addFormBtn.addEventListener("click", () => {
                    axios.post("https://news-blog-server-z5lf.onrender.com/posts", {
                        "author": author.value,
                        "date": new Date(),
                        "text": text.value,
                        "title": name.value,
                        "image": image.value
                    }).then((res) => {
                        axios.post("https://news-blog-server-z5lf.onrender.com/tags", tagsArr.map(e => ({
                            postId: res.data.id,
                            text: e
                        }))).then(() => {
                            modal.classList.remove("visible")
                            start()
                        })
                    })
                })
                addFormBtn.innerText = "Add Post"
                form.appendChild(addFormBtn)

                modal.appendChild(form)
            }

            addBtn.addEventListener('click', () => {
                renderModal()
            })
        } else {
            renderLogin()
        }
    } else {
        renderLogin()
    }
}

go()


